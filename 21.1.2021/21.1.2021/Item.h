#pragma once
#include <string>
class Item
{
public:

	//C'tor
	Item(std::string name, std::string serialNumber, double price);
	
	//D'tor
	virtual ~Item();

	//Getters
	std::string getName() const { return _name; };
	std::string getSerial() const { return _serialNumber; };
	int getCount() const { return _count; };
	double getPrice() const { return _unitPrice; };

	//Setters
	void setName(std::string name) { _name = name; };
	void setSerial(std::string serial) { _serialNumber = serial; };
	void setCount(int count) { _count = count; };
	void setPriced(double price) { _unitPrice = price; };

	//Methods
	double totalPrice() { return _count * _unitPrice; };
	bool operator<(Item const& other) const;
	bool operator>(Item& other);
	bool operator==(Item const& other) const;

private:
	std::string _name;
	std::string _serialNumber;
	int _count;//Default is 1, can never be less than 1
	double _unitPrice;//Always bigger than 0

};

