#include "Customer.h"

Customer::Customer(std::string name)
{
	_name = name;
}

Customer::Customer()
{
}

Customer::~Customer()
{
}

double Customer::totalSum() const
{
	
	double sum = 0;
	for (auto it : _items)
		sum += it.totalPrice();
	return sum;
}

void Customer::addItem(Item item)
{
	std::set<Item>::iterator it;
	Item temp = item;
	for (it = this->_items.begin(); it != this->_items.end(); it++)
	{
		if (*it == item)
		{
			temp.setCount(it->getCount() +1);
            this->_items.erase(item);
            this->_items.insert(temp);
			return;
		}
	}
	
	this->_items.insert(item);

}

void Customer::removeItem(Item item)
{
	std::set<Item>::iterator it;
	Item temp = item;
	for (it = this->_items.begin(); it != this->_items.end(); it++)
	{
		if (*it == item)
		{
			if (item.getCount() > 1)
			{
				temp.setCount(it->getCount() - 1);
				this->_items.erase(item);
				this->_items.insert(temp);
				return;
			}
			else
			{
				this->_items.erase(item);
				return;
			}
		}
	}
}

bool Customer::InBag(std::set<Item> items, int index)
{
	std::string serial = "";
	int counter = 0;
	for (auto it : items)
	{
		if (counter == index - 1)
		{
			serial = it.getSerial();
			break;
		}
		else
		{
			counter++;
		}
	}
	for (auto it : _items)
	{
		if (it.getSerial() == serial)
		{
			return true;
		}
	}
	return false;
}
