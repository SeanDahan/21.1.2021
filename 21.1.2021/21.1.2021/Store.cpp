#include "Store.h"

Store::Store()
{
	_products.insert(Item("Milk", "4891", 2.59));
	_products.insert(Item("Cookies", "2156", 3.01));
	_products.insert(Item("Water", "3498", 1.21));
	_products.insert(Item("Tomato", "1168", 3.63));
	_products.insert(Item("Cereal", "1651", 4.12));
	_products.insert(Item("Ice Cream", "6541", 5.36));
	_products.insert(Item("Steak", "1658", 12.4));
	_products.insert(Item("Candy", "1893", 1.26));
	_products.insert(Item("Chicken", "1891", 6.45));
	_products.insert(Item("Wine", "4556", 7.65));

}

Store::~Store()
{
}

void Store::SignUp()
{
	std::string newNameInput = "";
	bool flag = true;
	while (flag)
	{
		std::cout << "[ Enter your name please ]\n";
		std::cin >> newNameInput;
		for (auto it : _abcCustomers)
		{
			if (it.first == newNameInput)
			{
				std::cout << "{ Name Error : New name already exists in the system }\n";
				return;
			}
		}
		flag = false;
	}
	_abcCustomers[newNameInput] = new Customer(newNameInput);
	SignIn(newNameInput);
}

void Store::SignIn(std::string name)
{
	if (name == "")
	{
		bool flag = true;
		while (flag)
		{
			std::cout << "[ Please enter your name to sign in ]\n";
			std::cin >> name;
			for (auto it : _abcCustomers)
			{
				if (it.first == name)
				{
					flag = false;
					break;
				}
			}
		}
	}

	int choiceInput = -1;
	while (true)
	{
		if (_abcCustomers[name]->getItems().size() > 0)
		{
			int i = 1;
			for (auto it : _abcCustomers[name]->getItems())
			{
				std::cout << i++ << ". [ " << it.getName() << " ] with { " << it.getCount() << " }\n";
			}
		}
		do
		{
			std::cout << "[ Please enter what action would you like to do ]\n1. < Add Items > \n2. < Remove Items >\n3. < Back to Menu >\n^ Enter your decision^\n";
			std::cin >> choiceInput;
		}while (choiceInput > 3 || choiceInput < 0);
		switch (choiceInput)
		{
		case 1:AddItems(name);
			break;
		case 2: RemoveItems(name);
			break;
		case 3:
			return;
		}
	}
}


void Store::AddItems(std::string name)
{
	int productInput = 1;
	while (productInput != 0)
	{
		ProductList();
		std::cin >> productInput;
		if (productInput <= 10 && productInput > 0)
		{
			std::string serial = "";
			int counter = 0;
			for (auto it : _products)
			{
				if (counter == productInput - 1)
				{
					_abcCustomers[name]->addItem(it);
					break;
				}
				else
				{
					++counter;
				}
			}
		}
		else if(productInput != 0)
		{
			std::cout << "[ Index Error : Product index doesnt exist ]\n";
		}
	}
}

void Store::RemoveItems(std::string name)
{
	int removeOption = 1, i = 1;
	while (removeOption != 0 && _abcCustomers[name]->getItems().size() > 0)
	{
		i = 1;
		for (auto it : _abcCustomers[name]->getItems())
		{
			std::cout << i++ << ". [ " << it.getName() << " ] with { " << it.getCount() << " }\n";
		}
		std::cout << "[ Enter an item to remove ]\n";
		std::cin >> removeOption;
		int counter = 0;
		for (auto it : _abcCustomers[name]->getItems())
		{
			if (counter == removeOption - 1)
			{
				_abcCustomers[name]->removeItem(it);
				break;
			}
			else
			{
				++counter;
			}
		}
	}
}

void Store::GeneralMessage() const
{
	std::cout << "\n[ Welcome to MagshiMart! ]\n1. < Sign up as a new customer and buy items >\n2. < Sign in as a customer >\n3. < Print statistics >\n4. < Exit >\n ^ Enter your choice ^\n";
}

void Store::ProductList() const
{
	int i = 1;
	for (auto it : _products)
	{
		std::cout << i++ << ". [ " << it.getName() << " ] at { " << it.getPrice() << " }\n";
	}
}

void Store::PrintStatistics() const
{
	if (_abcCustomers.size() > 0)
	{
		std::string max = (*(_abcCustomers.begin())).second->getName();
		for (auto it : _abcCustomers)
		{
			if (it.second->totalSum() > _abcCustomers.at(max)->totalSum())
			{
				max = it.second->getName();
			}
		}
		std::cout << "[ Highest Paying customer in the store is " << _abcCustomers.at(max)->getName() << " With an amount of " << _abcCustomers.at(max)->totalSum() << " ]\n";

	}
}
