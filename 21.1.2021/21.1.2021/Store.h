#pragma once
#include <string>
#include "Customer.h"
#include <map>
#include <iostream>
class Store
{
public:
	//C'tor
	Store();

	//D'tor
	virtual ~Store();

	//Getters
	std::map<std::string, Customer*> getCustomers() const { return _abcCustomers; };
	std::set<Item> getProducts() const { return _products; };

	//Methods
	void SignUp();
	void SignIn(std::string name);
	void AddItems(std::string name);
	void RemoveItems(std::string name);

	//UI
	void GeneralMessage() const;
	void ProductList() const;
	void PrintStatistics() const;

private:
	std::map<std::string, Customer*> _abcCustomers;
	std::set<Item> _products;
};

