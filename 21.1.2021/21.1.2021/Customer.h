#pragma once
#include <iostream>
#include <string>
#include <set>
#include "Item.h"
class Customer
{
public:
	//C'tor
	Customer(std::string name);
	Customer();

	//D'tor
	virtual ~Customer();
	
	//Getters
	std::string getName() const { return _name; };
	std::set<Item> getItems() const { return _items; };

	//Setters
	void setName(std::string name) { _name = name; };
	void setItems(std::set<Item> items) { _items = items; };

	//Methods
	double totalSum() const;//Returns the total sum of payment
	void addItem(Item it);//Adds an item to the set
	void removeItem(Item it);//Removes a specific item from the set

	//Helpers
	bool InBag(std::set<Item> items, int index);

private:
	
	std::string _name;
	std::set<Item> _items;

};

