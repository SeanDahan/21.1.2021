#include "Item.h"

Item::Item(std::string name, std::string serialNumber, double price):
    _name{name}, _serialNumber{serialNumber}, _unitPrice{price}
{
    this->_count = 1;
}

Item::~Item()
{
}

bool Item::operator<(Item const& other) const
{
    return _serialNumber < other.getSerial();
}

bool Item::operator>(Item& other)
{
    return _serialNumber > other.getSerial();
}

bool Item::operator==(Item const& other) const
{
    return _serialNumber == other.getSerial();
}
